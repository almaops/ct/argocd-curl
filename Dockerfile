ARG ARGOCD_VERSION
FROM argoproj/argocd:${ARGOCD_VERSION}

USER root

RUN apt-get update -qq && \
    apt-get install -y curl && \
    apt-get clean

USER 999
